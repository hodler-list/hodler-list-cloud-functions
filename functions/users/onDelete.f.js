/* const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

exports = module.exports = firebaseFunctions.firestore.document('users/{userId}').onDelete(event => {
  const userId = event.params.userId;

  const batch = admin.firestore().batch();

  // TODO
  // Delete username, upvotes from this user, portfolio upvotes counter for this user
  // and portfolios made by this user

  // TODO: Must delete collection
  //const upvoteRef = admin.firestore().doc(`portfolio-upvotes/users/${userId}`)
  //batch.delete(upvoteRef);



  // TODO: Must delete collection
  // const upvoteCounterRef = admin.firestore().doc(`counters/portfolio-upvotes/users/${userId}`);
  // batch.delete(upvoteCounterRef);

  let promises = [];

  const usernameQuery = admin.firestore().collection('usernames').where('userId', '==', userId);
  promises.push(
    usernameQuery.get()
      .then(snapshots => {
        snapshots.forEach(doc => batch.delete(doc.ref));
      })
  );
  const portfoliosQuery = admin.firestore().collection('portfolios').where('userId', '==', userId);
  promises.push(
    portfoliosQuery.get()
      .then(snapshots => {
        snapshots.forEach(doc => batch.delete(doc.ref));
      })
  );
  return Promise.all(promises)
    .then(() => batch.commit());
});
 */