const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

const counter = require('../../utils/counter');

exports = module.exports = firebaseFunctions.firestore.document('users/{userId}').onCreate((event) => {
  const counterRef = admin.firestore().doc('counters/users');

  return counterRef.get()
    .then((snapshot) => {
      if (snapshot.exists) {
        return counter.incrementCounter(counterRef, counter.data.numShards);
      } else {
        return counter.createCounter(counterRef, counter.data.numShards)
          .then(() => {
            return counter.incrementCounter(counterRef, counter.data.numShards);
          });
      }
    });
});
