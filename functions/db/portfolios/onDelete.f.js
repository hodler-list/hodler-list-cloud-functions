const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

const counter = require('../../utils/counter');
const algolia = require('../../utils/algolia');

exports = module.exports = firebaseFunctions.firestore.document('portfolios/{portfolioId}').onDelete(event => {
  const portfolioId = event.params.portfolioId;
  const counterRef = admin.firestore().doc('counters/portfolios');

  // No need to check whether counter exists
  // because we are deleting a portfolio
  // so counter must have been already created
  // just decrease the counter

  return Promise.all([
    counter.decreaseCounter(counterRef, counter.data.numShards),
    algolia.portfolio.delete(portfolioId),
  ]);
});
