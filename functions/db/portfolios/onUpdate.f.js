const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

const algolia = require('../../utils/algolia');

exports = module.exports = firebaseFunctions.firestore.document('portfolios/{portfolioId}').onUpdate((event) => {
  const portfolioId = event.params.portfolioId;
  const { name, note } = event.data.data();

  return algolia.portfolio.update(portfolioId, name, note);
});
