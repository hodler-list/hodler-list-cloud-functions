const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

const counter = require('../../utils/counter');
const algolia = require('../../utils/algolia');

exports = module.exports = firebaseFunctions.firestore.document('portfolios/{portfolioId}').onCreate((event) => {
  const portfolio = event.data.data();
  const portfolioId = event.params.portfolioId;

  const portfolioToIndex = {
    name: portfolio.name,
    note: portfolio.note,
    coinNames: [],
    coinSymbols: [],
  };

  portfolio.coins.forEach(coin => {
    portfolioToIndex.coinNames.push(coin.name);
    portfolioToIndex.coinSymbols.push(coin.symbol);
  });

  const counterRef = admin.firestore().doc('counters/portfolios');

  return Promise.all([
    counterRef.get()
      .then((snapshot) => {
        if (snapshot.exists) {
          // Increment counter
          return counter.incrementCounter(counterRef, counter.data.numShards);
        } else {
          // Initialize counter
          // then increment counter
          return counter.createCounter(counterRef, counter.data.numShards)
            .then(() => {
              return counter.incrementCounter(counterRef, counter.data.numShards);
            });
        }
      }),
    algolia.portfolio.index(portfolioId, portfolioToIndex),
  ]);

});
