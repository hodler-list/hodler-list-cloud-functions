const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

const counter = require('../../../utils/counter');

exports = module.exports = firebaseFunctions.firestore.document('portfolio-comments/users/{userId}/{commentId}').onDelete((event) => {
  const userId = event.params.userId;
  const counterRef = admin.firestore().doc(`counters/portfolio-comments/users/${userId}`);

  return counter.decreaseCounter(counterRef, counter.data.numShards);
});
