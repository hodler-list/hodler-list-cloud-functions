const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

const algolia = require('../../utils/algolia');

exports = module.exports = firebaseFunctions.firestore.document('coins/{coinId}').onUpdate(event => {
  const coinId = event.params.coinId;
  const { name, symbol } = event.data.data();

  return algolia.coin.update(coinId, name, symbol);
});