const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

const counter = require('../../../utils/counter');

exports = module.exports = firebaseFunctions.firestore.document('portfolio-upvotes/portfolios/{portfolioId}/{userId}').onDelete((event) => {
  const portfolioId = event.params.portfolioId;
  const counterRef = admin.firestore().doc(`counters/portfolio-upvotes/portfolios/${portfolioId}`);

  return counter.decreaseCounter(counterRef, counter.data.numShards);
});
