const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

module.exports = {
  data: {
    numShards: 3,
  },
  createCounter(ref, numShards) {
    const batch = admin.firestore().batch();

    // Initialize the counter document
    batch.set(ref, { numShards });

    // Initialize each shard with count = 0
    for (let i = 0; i < numShards; i++) {
      const shardRef = ref.collection('shards').doc(i.toString());
      batch.set(shardRef, { count: 0 });
    }

    return batch.commit();
  },
  incrementCounter(ref, numShards) {
    // Randomly select a counter shard
    const shardId = Math.floor(Math.random() * numShards).toString();
    const shardRef = ref.collection('shards').doc(shardId);

    // Update count
    return admin.firestore().runTransaction((t) => {
      return t.get(shardRef)
        .then((doc) => {
          const newCount = doc.data().count + 1;
          t.update(shardRef, { count: newCount });
        });
    });
  },
  decreaseCounter(ref, numShards) {
    // Randomly select a counter shard
    const shardId = Math.floor(Math.random() * numShards).toString();
    const shardRef = ref.collection('shards').doc(shardId);

    // Update count
    return admin.firestore().runTransaction((t) => {
      return t.get(shardRef)
        .then((doc) => {
          const newCount = doc.data().count - 1;
          t.update(shardRef, { count: newCount });
        });
    });
  },
  getCount(ref) {
    return ref.collection('shards').get()
      .then((snapshot) => {
        let totalCount = 0;
        snapshot.forEach((doc) => {
          totalCount += doc.data().count;
        });
        return totalCount;
      });
  },
};