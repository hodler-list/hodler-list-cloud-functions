const firebaseFunctions = require('firebase-functions');
const admin = require('firebase-admin');
try { admin.initializeApp(firebaseFunctions.config().firebase); } catch (e) { } // You do that because the admin SDK can only be initialized once.

const counter = require('./counter');

module.exports = {
  addPortfolioToTopByUpvotes(portfolioId) {
    // Get upvote count for this portfolio
    const portfolioCounterRef = admin.firestore().doc(`counters/portfolio-upvotes/portfolios/${portfolioId}`);
    return counter.getCount(portfolioCounterRef)
      .then((count) => {
        // Add portfolio to 'top-portfolios-today/by-upvote/portfolioId'
        const topPortfolioRef = admin.firestore().doc(`top/portfolios/by-upvotes/${portfolioId}`);
        return topPortfolioRef.set({ count });
      });
  },
  deleteFromTopByUpvotes(portfolioId) {

  },
};
