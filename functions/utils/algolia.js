const firebaseFunctions = require('firebase-functions');

const algoliasearch = require('algoliasearch');
const ALGOLIA_ID = firebaseFunctions.config().algolia.app_id;
const ALGOLIA_ADMIN_KEY = firebaseFunctions.config().algolia.api_key;
const ALGOLIA_SEARCH_KEY = firebaseFunctions.config().algolia.search_key;
const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY);

module.exports = {
  coin: {
    index(objectID, coinData) {
      const indexName = 'coins';

      const coin = coinData;
      coin.objectID = objectID;

      const index = client.initIndex(indexName);
      return index.saveObject(coin);
    },
    delete(objectID) {
      const indexName = 'coins';
      const index = client.initIndex(indexName);
      return index.deleteObject(objectID);
    },
    update(objectID, newName, newSymbol) {
      const indexName = 'coins';
      const index = client.initIndex(indexName);
      return index.partialUpdateObject({
        objectID,
        name: newName,
        symbol: newSymbol,
      });
    },
  },

  portfolio: {
    index(objectID, portfolioData) {
      const indexName = 'portfolios';

      const portfolio = portfolioData;
      portfolio.objectID = objectID;

      const index = client.initIndex(indexName);
      return index.saveObject(portfolio);
    },
    delete(objectID) {
      const indexName = 'portfolios';
      const index = client.initIndex(indexName);
      return index.deleteObject(objectID);
    },
    update(objectID, newName, newNote) {
      const indexName = 'portfolios';
      const index = client.initIndex(indexName);
      return index.partialUpdateObject({
        objectID,
        name: newName,
        note: newNote,
      });
    },
  },
};
